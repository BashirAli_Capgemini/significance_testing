import requests as re
import YouScanConfig as _config

def get_topics(apikey, **kwargs):
    """
    retrieves topics available for api token
    :param apikey: api token
    :param kwargs: additional optional arguments
    :return:
    """
    payload = {'apikey': apikey}
    response = re.get('https://api.youscan.io/api/external/topics', params=payload)
    return response.json()

def do(payload, config, plugin_config, inputs):
    """
    Do method for front end python setup
    :param payload: parameters passed from javaScript
    :param config: Dataiku parameter
    :param plugin_config: Dataiku parameter
    :param inputs: Dataiku parameter
    :return: Either returns a successful response containing topic names + id or returns error message
    """
    api_key = _config.YOUSCAN_API_KEY
    if payload['get_topics']:
        try:
            response = get_topics(api_key)

            return {"status": "SUCCESS",
                    "response": response['topics']}  # this can be a dict as it will be
        except KeyError as e:
            return {"status": "ERROR",
                    "message": "Connection to api cannot be established: " + str(e)}
    else:
        #other front end interaction - none other required for now
        pass