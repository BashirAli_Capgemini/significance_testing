import YouScanAPI as youscan
import YouScanConfig as config
import YouScanSetup as setup

from plugin_utils.logging import get_logger

logger = get_logger(setup=True)

# Retrieve API token using stored credentials
api_token = config.YOUSCAN_API_KEY
# Retrieve other necessary data
youscan_columns = config.YOUSCAN_SCHEMA
logger.info("API token retrieved")

# Get recipe config values
start_date, end_date = setup.get_dates()
topic_id = setup.get_chosen_topic_id()

output = setup.get_output()
setup.clear_output(output=output)
logger.info("Output cleared")

mentions_df = youscan.paging(topic_id, api_token, start_date, end_date, youscan_columns)

with output.get_writer() as writer:
        try:
            output.write_schema_from_dataframe(mentions_df)
            writer.write_dataframe(mentions_df)
            logger.info(str(len(mentions_df)) + " rows written to Dataset")
        except Exception as e:
            logger.info("Error - Data cannot be written to Dataiku: " + str(e))
