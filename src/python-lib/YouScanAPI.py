import requests as re
import pandas as pd
from plugin_utils.logging import get_logger

logger = get_logger(setup=True)

def create_payload(api_token, start_date, end_date):
    """
    creates the url parameters to call api
    :param api_token: api token
    :param start_date: date range
    :param end_date: date range
    :return: returns dictionary containing parameters
    """
    payload = {'apikey': api_token,
               'from': start_date,
               'to': end_date}
    logger.info("payload has been created")
    return payload

def create_mentions_url(topic_id):
    """
    creates api url to retrieve mentions for the topic with topic_id
    :param topic_id: id of chosen topic
    :return: url to be called
    """
    id = str(int(topic_id))
    topic_url = "https://api.youscan.io/api/external/topics/"
    mentions_url = topic_url + id + "/mentions"
    logger.info("api url for mentions has been created")
    return mentions_url


def get_json_response(payload, mentions_url, paging_payload):
    """
    calls api and retrieves the response
    :param payload: payload parameters for api
    :param paging_payload: additional batch data to append to api parameters
    :param mentions_url: api url
    :return: returns the response object
    """
    final_payload = dict(payload, **paging_payload)

    response = re.get(mentions_url, params=final_payload)
    logger.info("response url")
    logger.info(response.url)
    logger.info("json response:")
    logger.info(response)
    logger.info("status code:" + str(response.status_code))
    current_status_code = response.status_code
    json_response = {}
    if response.status_code != 200:
        logger.info("ERROR: Connection to the API has failed, please try again later. If the problem persists, contact PDC Services and Capabilities with the status code: "
                        + str(response.status_code))
    else:
        json_response = response.json()
    #json_response = response.json()
    logger.info("response has been retrieved from api")
    #lastSeq parameter needs to be retrieved manually:
    last_dict_mention = json_response['mentions'][len(json_response['mentions']) - 1]
    last_seq = last_dict_mention['seq']
    return json_response, current_status_code, json_response['total'], len(json_response['mentions']), last_seq

def json_to_df(response_mentions, columns):
    """
    changes typical json response containing mentions in to a dataframe
    :param json_response: response object
    :param columns: data columns that is required in the output
    :return: dataframe
    """
    try:
        df = pd.DataFrame(response_mentions, columns=columns)
    except:
        df = pd.DataFrame(columns=columns)

    return df

def merge_df(list_of_df):
    """
    merges a list of dfs in to a large df
    :param list_of_df: the list of available df
    :return: large df
    """
    return pd.concat(list_of_df, axis=0)

def paging(topic_id, api_token, start_date, end_date, youscan_columns):
    # paging information to retrieve all mentions
    batch_size = 1000
    hasNextPage = True
    list_of_response_df = []
    seq_id = 0

    logger.info("Starting download for chosen topic")
    # api preparation: payload, url and total mentions
    mentions_payload = create_payload(api_token, start_date, end_date)
    mentions_api_url = create_mentions_url(topic_id)
    #paging process with batches of 1000 until all mentions are processed
    while hasNextPage:
        try:
            response, current_status_code, total_mentions, mention_size, lastSeq = get_json_response(
                mentions_payload, mentions_api_url, {'size': batch_size, 'sinceSeq': seq_id, 'orderBy': 'seqAsc'})
            logger.info("Returned response with total remaining mentions of: " + str(total_mentions))
            logger.info("Mentions in this batch: " + str(mention_size))
        except Exception as e:
            logger.info("Something went wrong when accessing the API with the message: ")
            logger.info(e)

        seq_id = lastSeq
        df = json_to_df(response['mentions'], youscan_columns)
        list_of_response_df.append(df)
        remaining_mentions = total_mentions - mention_size
        logger.info("Mentions remaining to process: " + str(remaining_mentions))
        # if there is no mentions left
        if (remaining_mentions == 0) or (current_status_code != 200):
            # print("entered if statement")
            # exit the loop
            hasNextPage = False

    # Write schema if the list of df is not empty
    if list_of_response_df:
        mentions_df = merge_df(list_of_response_df)
        logger.info(str(len(list_of_response_df)) + " Dataframes created.")

        logger.info("Dataframe rows = " + str(len(mentions_df)))
    else:
        logger.info("Dataset has not been written as dataframes list created using paging is empty")

    return mentions_df