from __future__ import absolute_import
import logging


def get_logger(setup=True, log_level=logging.INFO):
    log_format = "%(asctime)s *** %(levelname)-8s - %(module)25s - %(funcName)-30s[%(lineno)3s]: %(message)s"

    logger = logging.getLogger(__name__)

    if setup:
        logger.setLevel(logging.DEBUG)
        logger.handlers = []  # remove default Dataiku handlers

        ch = logging.StreamHandler()
        ch.setLevel(log_level)

        formatter = logging.Formatter(log_format)
        ch.setFormatter(formatter)

        logger.addHandler(ch)  # Add new one

    return logger
