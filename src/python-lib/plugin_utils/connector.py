from abc import ABCMeta, abstractmethod

from plugin_utils.logging import get_logger

logger = get_logger(setup=True)


class DataikuConnectorBase:
    __metaclass__ = ABCMeta

    def __init__(self, config, plugin_config):
        self.config = config
        self.plugin_config = plugin_config

    def get_read_schema(self):
        column_list = self.get_columns()

        return {
            "columns": column_list
        }

    def generate_rows(self, dataset_schema=None, dataset_partitioning=None, partition_id=None, records_limit=-1):
        results = self.get_dataframe()
        results_list = results.to_dict(orient='records')

        for row in results_list:
            yield row

    @abstractmethod
    def get_dataframe(self, *args):
        pass

    @abstractmethod
    def get_columns(self):
        pass
