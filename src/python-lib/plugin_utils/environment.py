import dataiku


environment_variable_defaults = {
    "plugin.logging.default_format": "*** %(levelname)-8s - %(module)25s - %(funcName)-30s[%(lineno)3s]: %(message)s"
}


def get_variables():
    try:
        environment_variables = dataiku.get_custom_variables()
    except Exception:
        # not supported within dataiku dataset plugins for now - it'll raise an exception
        # n.b. dataiku only raise an exception - nothing more specific to catch
        environment_variables = {}

    context = environment_variable_defaults.copy()  # default variables
    context.update(environment_variables)  # ones from the user

    return context