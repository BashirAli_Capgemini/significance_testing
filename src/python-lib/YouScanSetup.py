import dataiku
from dataiku.customrecipe import get_recipe_config, get_output_names_for_role
import pandas as pd
import YouScanConfig as config
# -*- coding: utf-8 -*-


def get_dates():
    try:
        start_date = get_recipe_config()['startDate']
    except KeyError:
        raise Exception("ERROR: No date entered for 'Start'. Please select a date and re-run the recipe.")

    try:
        end_date = get_recipe_config()['endDate']
    except KeyError:
        raise Exception("ERROR: No date entered for 'End'. Please select a date and re-run the recipe.")

    return start_date, end_date

def get_chosen_topic_id():
    try:
        chosen_topic_id = get_recipe_config()['selected_topic']
    except KeyError:
        raise Exception("ERROR: No topic chosen. Please select a topic and re-run the recipe.")

    return chosen_topic_id

def get_output():
    output_name = get_output_names_for_role("output")[0]
    output = dataiku.Dataset(output_name)

    return output


def clear_output(output):
    empty_bw_df = pd.DataFrame(columns=config.YOUSCAN_SCHEMA)

    print("\nClearing output...\n")
    output.write_with_schema(empty_bw_df)

    return None
