var app = angular.module("youscanDownloader", []);

app.controller("YouScanFormController", function($scope) {

    $scope.retrieveTopics = function () {
        $scope.callPythonDo({'get_topics': true}).then(function (data) {
            //$scope.callPythonDo({'action': 'get_topics'}).then(function (data) {
            // success
            $scope.ResponseStatus = data.status;
            if ($scope.ResponseStatus === 'SUCCESS') {
            $scope.available_topics = data.response;
                        }
            }, function (data) {
                // failure
                $scope.ResponseStatus = 'FAILED';
                console.log("callPythonDo function FAILED!");
            });
    }

   $scope.checkDates = function (start, end) {
        var startDate = new Date(start);
        var endDate = new Date(end);
        var today = new Date();

        if (startDate > endDate) {
            $scope.dateError = true;
        } else {
            $scope.dateError = false;
        }

        if (startDate > today) {
            $scope.startDateFuture = true;
        } else {
            $scope.startDateFuture = false;
        }

        if (endDate > today) {
            $scope.endDateFuture = true;
        } else {
            $scope.endDateFuture = false;
        }
    };

    $scope.checkDates($scope.config.startDate, $scope.config.endDate);
});